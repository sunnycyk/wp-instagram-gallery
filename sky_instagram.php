<?php
/* 
Plugin Name:  Sunny Instagram API Plugin
Plugin URI: http://sunnycyk.hk	
Description: Create custom gallery, retrieve photo from instagram
Author: Sunny Cheung	
Version: 1.01
Author URI: http://sunnycyk.hk
*/

// Action for activation or deactivation

register_activation_hook(__FILE__, 'skyc_instagram_install');

function skyc_instagram_install() {
	// Add all option for use later in plugin
	add_option('skyc_instagram_accesstoken', false);
	add_option('skyc_instagram_user', false);
	add_option('skyc_instagram_client_id', false);
	add_option('skyc_instagram_client_secret', false);
}

register_deactivation_hook(__FILE__, 'skyc_instagram_uninstall');

function skyc_instagram_uninstall() {
	delete_option('skyc_instagram_accesstoken');
	delete_option('skyc_instagram_user');
	delete_option('skyc_instagram_client_id');
	delete_option('skyc_instagram_client_secret');
}

// End Activation Section

// Add Short Code
add_shortcode('my_instagram','skyc_instagram');

function skyc_instagram($atts, $content=null) {
	$access_token = get_option('skyc_instagram_accesstoken');

	if (!$access_token) {
?>
		<div> Opp! Seem like your instgram API is not ready! </div>
<?php
	} else {
		$user = unserialize(get_option('skyc_instagram_user'));
		
?>
<div id="skyc_instagram_gallery">
		<img src="<?php echo $user->profile_picture; ?>" alt="Profile Picture" />
		<h3><a href="http://instagram.com/<?php echo $user->username; ?>"><?php echo $user->full_name; ?></a></h3>
				
<?php		
		$url = "https://api.instagram.com/v1/users/".$user->id."/media/recent/?access_token=" . $access_token . "&count=20";
		$args = array(
			'method' => 'GET',
			'sslverify' => false
		);
		
		$response = wp_remote_get($url, $args);
		if (is_wp_error($response)) {
?>		
		<div> Opp! Something is wrong! Error while connecting to Instgram!</div>
<?php
		}
		else {
			if ($response['response']['code'] == 200) { // code ok
				$json_obj = json_decode($response['body']);
				foreach ($json_obj->data as $image) {
?>
					<img src="<?php echo $image->images->thumbnail->url; ?>" alt="<?php echo $image->caption->text; ?>" />
<?php
				}
?>
				
<?php
			}
			else { // must be something wrong 
?>
		
				<div> Opp! Something is wrong! cannot fetch data!</div>
<?php
			}
		}
?>
</div>
<?php
	} // else
}

// End Short Code

// Plugin Admin Menu

add_action('admin_menu', 'skyc_instagram_menu'); 

function skyc_instagram_menu() {
	add_menu_page('Instagram Gallery Setting Page', 'Instagram Gallery Setting Page', 'manage_options','skyc_instagram_plugin',	
		'skyc_instagram_settings_page'); 
}


// version 1.01
function skyc_instagram_settings_page() {
	if (!isset($_REQUEST['skyc_instagram_action'])) {
		$access_token = get_option('skyc_instagram_accesstoken');
		
		if (!$access_token) {  // seem token is not set yet
			if (isset($_REQUEST['code'])) { // This is possible received from instagram
				$client_id = get_option('skyc_instagram_client_id');
				$client_secret = get_option('skyc_instagram_client_secret');
				if (!$client_id && !$client_secret) {
					add_action('skyc_instagram_message', 'skyc_error_notice',10,1);
					skyc_instagram_client_setting_page("Client ID or Client Secrect cannot be empty");
					return;
				}
				$url = 'https://api.instagram.com/oauth/access_token';
				$args = array(
					'method' => 'POST',
					'body' => array(
						'client_id' =>  $client_id,
						'client_secret' => $client_secret,
						'grant_type' => 'authorization_code',
						'redirect_uri' => admin_url('admin.php?page=skyc_instagram_plugin', 'http'),
						'code' => $_REQUEST['code']
					),
					'sslverify' => false
				);
				$response = wp_remote_post($url, $args);
				if (is_wp_error($response)) {
					add_action('skyc_instagram_message', 'skyc_error_notice');
					skyc_instagram_authorize_instagram_page('Error when retrieving Access Token from Instagram.  Reset Client Information and try again.');
					return;
				}
				else {
					
					if ($response['response']['code'] == 200) {
						$json_obj = json_decode($response['body']);
						$access_token = $json_obj->access_token;
						$instagram_user = $json_obj->user;
						// need to delete option because option is not ready
						delete_option('skyc_instagram_user');
						add_option('skyc_instagram_user', serialize($instagram_user));	
						delete_option('skyc_instagram_accesstoken');
						add_option('skyc_instagram_accesstoken', $access_token);	
						add_action('skyc_instagram_message', 'skyc_updated_notice');	
						skyc_instagram_client_info_page("You can access to Instagram API!  Ready to play with it?");	
					}
					else {
						add_action('skyc_instagram_message', 'skyc_error_notice');
						skyc_instagram_authorize_instagram_page('Error when retrieving Access Token from Instagram.  Reset Client Information and try again.');
						return;
					}
					
				}
			}
			else {
				skyc_instagram_client_setting_page();
			}
		}
		else {		
			add_action('skyc_instagram_message', 'skyc_updated_notice');	
			skyc_instagram_client_info_page("You can access to Instagram API!  Ready to play with it?");	
		}
	}
	else {
		
		if(!current_user_can('manage_options')) {
			wp_die("Insufficient privileges!");
		}
		$action = $_REQUEST['skyc_instagram_action'];
	
		check_admin_referer('skyc_instagram_action-'.$action);
		switch ($action) {
			case 'resetClient': update_option('skyc_instagram_accesstoken', false);
								update_option('skyc_instagram_user', false);
								update_option('skyc_instagram_client_id', false);
								update_option('skyc_instagram_client_secret', false);
								add_action('skyc_instagram_message', 'skyc_updated_notice');
								skyc_instagram_client_setting_page('Client Information has been reseted');
								break;
			case 'setClient'  : if (($_REQUEST['instagram_client_id'] == "") || ($_REQUEST['instagram_client_secret'] == "")) {
									add_action('skyc_instagram_message', 'skyc_error_notice',10,1);
									skyc_instagram_client_setting_page("Client ID or Client Secrect cannot be empty");
									break;
								}			
								update_option('skyc_instagram_client_id', esc_attr($_REQUEST['instagram_client_id']));
							    update_option('skyc_instagram_client_secret', esc_attr($_REQUEST['instagram_client_secret']));
								add_action('skyc_instagram_message', 'skyc_updated_notice');
								skyc_instagram_authorize_instagram_page('Client Information Saved');
								break;
		}
		
		
	}
}

function skyc_instagram_client_info_page($msg='') {
	$user = unserialize(get_option('skyc_instagram_user'));
?>
	<div class="wrap">
		<header>
			<?php screen_icon('tools');?>
			<h2>Instagram Gallery Settings</h2>
			<?php do_action('skyc_instagram_message', $msg); ?>
			 <?php settings_errors(); ?>
		</header>
		<div>
			<p><strong>Profile Image:</strong></p>
			<p> <img src="<?php echo $user->profile_picture; ?>" alt="Profile Picture" /></p>
			<p><strong>User ID: <?php echo $user->id; ?></strong></p>
			<p><strong>User Name: <?php echo $user->username; ?></strong></p>
			<p><strong>User Full Name: </strong><?php echo $user->full_name; ?></p>
			<form action="<?php echo admin_url('admin.php?page=skyc_instagram_plugin', 'http');?>" method="POST">
			<?php	wp_nonce_field('skyc_instagram_action-resetClient'); ?>
			<input name="skyc_instagram_action" type="hidden" value="resetClient" />
			<input name="Submit" type="submit" value="Reset Instagram" class="button-primary" />
			</form>
		</div>
	</div>
<?php
	
}

function skyc_error_notice($msg) {
	echo '<div class="error">'.$msg.'</div>';
}



function skyc_updated_notice($msg) {
	echo '<div class="updated">'.$msg.'</div>';
}


function skyc_instagram_authorize_instagram_page($msg='') {
?>
	<div class="wrap">
		<header>
			<?php screen_icon('tools');?>
			<h2>Instagram Gallery Settings</h2>
			<?php do_action('skyc_instagram_message', $msg); ?>
			 <?php settings_errors(); ?>
		</header>
	<div>
		<h3>Click Button "Authorized from Instagram" to grand access to Instagram API or Click "Reset Instagram Data" to reset previous saved Instagram Data.</h3>
		  <?php settings_errors(); ?>
		<p><strong>Client ID: </strong> <?php echo get_option('skyc_instagram_client_id'); ?></p>
		<p><strong>Client Secret: </strong> <?php echo get_option('skyc_instagram_client_secret'); ?></p>
		<p>
		<form action="https://api.instagram.com/oauth/authorize/?" method="get">
			<input name="client_id" type="hidden" value="<?php echo get_option('skyc_instagram_client_id'); ?>"/>
			<input name="response_type" type="hidden" value="code" />
			<input name="redirect_uri" type="hidden" value="<?php echo admin_url('admin.php?page=skyc_instagram_plugin', 'http'); ?>" />
			<input name="Submit" type="submit" value="Authorized from Instagram" class="button-primary" />
	
		</form>
		</p><p><form action="<?php echo admin_url('admin.php?page=skyc_instagram_plugin', 'http');?>" method="POST">
			<?php	wp_nonce_field('skyc_instagram_action-resetClient'); ?>
			<input name="skyc_instagram_action" type="hidden" value="resetClient" />
			<input name="Submit" type="submit" value="Reset Instagram Data" class="button-primary" />
		</form>
		</p>
	</div>
<?php
}

function skyc_instagram_client_setting_page($msg='') {
?>
	<div class="wrap">
		<header>
			<?php screen_icon('tools');?>
			<h2>Instagram Gallery Settings</h2>
			<?php do_action('skyc_instagram_message', $msg); ?>
			 <?php settings_errors(); ?>
		</header>
		<div>
		 
		<form action="" method="post">
<?php
	settings_fields('skyc_instagram_api_options'); 
	do_settings_sections('skyc_instagram_plugin');
	wp_nonce_field('skyc_instagram_action-setClient');
?>
			<br />
			
			<input name="skyc_instagram_action" type="hidden" value="setClient" />
			<input name="Submit" type="submit" value="Save Changes" class="button-primary" />
	
		</form>
		</div>
	</div>
<?php
}


add_action('admin_init', 'skyc_instagram_api_admin_init');
function skyc_instagram_api_admin_init(){
	register_setting('skyc_instagram_api_options', 'instagram_client_id');
	register_setting('skyc_instagram_api_options', 'instagram_client_secret');
	add_settings_section('skyc_instagram_api_main', 'Instagram API Client ID and Secret Settings', 'skyc_instagram_api_section', 'skyc_instagram_plugin');
	add_settings_field('instagram_client_id', 'Enter Client ID','skyc_instagram_api_client_id_input','skyc_instagram_plugin', 'skyc_instagram_api_main');		   
	add_settings_field('instagram_client_secret', 'Enter Client Secret','skyc_instagram_api_client_secret_input','skyc_instagram_plugin', 'skyc_instagram_api_main');
	 
}

function skyc_instagram_api_section() {
	echo '<p>Enter your Client Information here. If you don\'t know your Client ID and Secret, you can register one at <a href="http://instagram.com/developer">http://instagram.com/developer</a></p>';
	echo '<p>You may need the follower information:</p>';
	echo '<p>Website URL: <strong>'.home_url('/').'</strong></p>';
	echo '<p>Redirect URI: <strong>'.admin_url('admin.php?page=skyc_instagram_plugin').'</strong></p>';
	
}

function skyc_instagram_api_client_id_input() {
	$options = get_option('skyc_instagram_client_id');	
?>
<input id="client_id" name="instagram_client_id" type="text" value="<?php echo $options; ?>" />
<?php
}


function skyc_instagram_api_client_secret_input() {
	$options = get_option('skyc_instagram_client_secret');
?>
<input id="client_secret" name="instagram_client_secret" type="text" value="<?php echo $options; ?>" />
<?php
}

function skyc_admin_notice() {
	if (!get_option('skyc_instagram_accesstoken')) {
		echo '<div class="error">You have activated Instagram Plugin, but you have not finish the setup yet.  Please <a href="admin.php?page=skyc_instagram_plugin">authorize</a>
			Instagram Plugin. </div>';
	}
}
add_action('admin_notices', 'skyc_admin_notice');
// End Plugin Admin Page
?>
